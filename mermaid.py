
plain = "wettervorhersagebern"
cypher = "rfzhzayuwxlinemmwneb"

print("paste this onto https://mermaid.live")

print("graph TD")
for (ind, (plain, cypher)) in enumerate(zip(plain, cypher)):
    print(f"\t {plain} --> |{ind+1}| {cypher}")
