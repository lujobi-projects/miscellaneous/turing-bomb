use std::collections::{HashMap, HashSet};

const ROTOR_SIZE: usize = 26;
const ROTOR_COUNT: usize = 3;

const fn char_of_usize(i: usize) -> char {
    (i as u8 + 'A' as u8) as char
}

const fn usize_of_char(c: char) -> usize {
    c as usize - 'A' as usize
}

#[derive(Debug, Clone, Copy)]
struct Rotor {
    forward_lookup: [usize; ROTOR_SIZE],
    backward_lookup: [usize; ROTOR_SIZE],
    name: char,
}

impl Rotor {
    pub const fn forward_pass(&self, in_ind: usize) -> usize {
        self.forward_lookup[in_ind]
    }
    pub const fn backward_pass(&self, in_ind: usize) -> usize {
        self.backward_lookup[in_ind]
    }
    pub fn new(wiring: &str, name: &char) -> Self {
        if wiring.len() != ROTOR_SIZE {
            panic!("Rotor wiring must be ROTOR_SIZE characters long");
        }
        let mut forward_lookup = [0; ROTOR_SIZE];
        let mut backward_lookup = [0; ROTOR_SIZE];
        for (i, c) in wiring.chars().enumerate() {
            let c = c as usize - 'A' as usize;
            forward_lookup[i] = c;
            backward_lookup[c] = i;
        }
        Rotor {
            forward_lookup,
            backward_lookup,
            name: *name,
        }
    }
}

impl std::fmt::Display for Rotor {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}

#[derive(Debug, Clone, Copy)]
struct DoubleScrambler {
    rotors: [Rotor; ROTOR_COUNT],
    reflector: Rotor,
    rotor_positions: [usize; ROTOR_COUNT],
}

impl DoubleScrambler {
    pub fn step(&mut self) {
        let mut add_step = true;
        for i in 0..ROTOR_COUNT {
            if !add_step {
                return;
            }
            let new_pos = (self.rotor_positions[i] + 1) % ROTOR_SIZE;
            self.rotor_positions[i] = new_pos;
            add_step = new_pos == 0;
        }
    }

    pub fn step_count(&mut self, count: usize) {
        for _ in 0..count {
            self.step();
        }
    }

    pub fn pass(&self, mut in_ind: usize) -> usize {
        for i in 0..ROTOR_COUNT {
            in_ind = (self.rotors[i].forward_pass((in_ind + self.rotor_positions[i]) % ROTOR_SIZE)
                + ROTOR_SIZE
                - self.rotor_positions[i])
                % ROTOR_SIZE;
        }
        in_ind = self.reflector.forward_pass(in_ind);
        for i in (0..ROTOR_COUNT).rev() {
            in_ind = (self.rotors[i]
                .backward_pass((in_ind + self.rotor_positions[i]) % ROTOR_SIZE)
                + ROTOR_SIZE
                - self.rotor_positions[i])
                % ROTOR_SIZE;
        }
        in_ind
    }

    pub fn pass_char(&self, c: char) -> char {
        char_of_usize(self.pass(usize_of_char(c)))
    }

    pub fn reset(&mut self) {
        for i in 0..ROTOR_COUNT {
            self.rotor_positions[i] = 0;
        }
    }
}

impl std::fmt::Display for DoubleScrambler {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "DS {}-{} [{}]",
            self.reflector,
            self.rotors.map(|r| format!("{}", r.name)).join(""),
            self.rotor_positions.map(|r| format!("{}", r)).join(",")
        )
    }
}

#[derive(Debug, Clone)]
struct DoubleScramblerConfigurations {
    rotors: Vec<Rotor>,
    reflectors: Vec<Rotor>,

    current_reflecor: usize,
    current_rotors: [usize; ROTOR_COUNT],
    _all_completded: bool,
}

impl DoubleScramblerConfigurations {
    pub fn new(rotors: Vec<Rotor>, reflectors: Vec<Rotor>) -> Self {
        DoubleScramblerConfigurations {
            rotors,
            reflectors,
            current_reflecor: 0,
            current_rotors: [0; ROTOR_COUNT],
            _all_completded: false,
        }
    }
}

impl Iterator for DoubleScramblerConfigurations {
    type Item = DoubleScrambler;
    fn next(&mut self) -> Option<Self::Item> {
        if self._all_completded {
            return None;
        }
        let res = DoubleScrambler {
            reflector: self.reflectors[self.current_reflecor],
            rotors: self.current_rotors.map(|f| self.rotors[f]),
            rotor_positions: [0; ROTOR_COUNT],
        };

        for i in 0..ROTOR_COUNT {
            self.current_rotors[i] = (self.current_rotors[i] + 1) % self.rotors.len();
            if self.current_rotors[i] != 0 {
                break;
            }
        }
        if self.current_rotors[ROTOR_COUNT - 1] == 0 {
            self.current_reflecor = (self.current_reflecor + 1) % self.reflectors.len();
        }
        if self.current_reflecor == 0 && self.current_rotors.iter().all(|&x| x == 0) {
            self._all_completded = true;
        }

        Some(res)
    }
}

#[derive(Debug, Clone)]
struct Loop(pub Vec<usize>);
#[derive(Debug, Clone)]
struct MessageAnalysis {
    loops: Vec<Loop>,
    ds_configs: DoubleScramblerConfigurations,
    ds_steps: HashMap<usize, usize>,
}
impl MessageAnalysis {
    pub fn new(loops: Vec<Loop>, ds_configs: DoubleScramblerConfigurations) -> Self {
        MessageAnalysis {
            loops: loops.clone(),
            ds_configs,
            ds_steps: loops
                .iter()
                .flat_map(|x| x.clone().0)
                .collect::<HashSet<_>>()
                .iter()
                .enumerate()
                .map(|(i, &x)| (x, i))
                .collect(),
        }
    }

    pub fn search_sols(&self, test_register: usize) -> Vec<DoubleScrambler> {
        let mut solution = vec![];
        for mut ds in self.ds_configs.clone() {
            ds.reset();

            let mut scramblers = self
                .ds_steps
                .keys()
                .map(|&step| {
                    let mut x = ds;
                    x.step_count(step);
                    x
                })
                .collect::<Vec<_>>();

            loop {
                if self.loops.iter().all(|l| {
                    let mut register = test_register;
                    for &step in l.0.iter() {
                        register = scramblers[self.ds_steps[&step]].pass(register);
                    }
                    register == test_register
                }) {
                    solution.push(ds);
                }

                ds.step();
                scramblers.iter_mut().for_each(|x| x.step());

                if ds.rotor_positions.iter().all(|&x| x == 0) {
                    break;
                }
            }
        }
        solution
    }
}

fn main() {
    let i_rotor = Rotor::new("EKMFLGDQVZNTOWYHXUSPAIBRCJ", &'1');
    let ii_rotor = Rotor::new("AJDKSIRUXBLHWTMCQGZNPYFVOE", &'2');
    let iii_rotor = Rotor::new("BDFHJLCPRTXVZNYEIWGAKMUSQO", &'3');
    let iv_rotor = Rotor::new("ESOVPZJAYQUIRHXLNFTGKDCMWB", &'4');
    let v_rotor = Rotor::new("VZBRGITYUPSDNHLXAWMJQOFECK", &'5');
    let ukw_a = Rotor::new("EJMZALYXVBWFCRQUONTSPIKHGD", &'A');
    let ukw_b = Rotor::new("YRUHQSLDPXNGOKMIEBFZCWVJAT", &'B');
    let ukw_c = Rotor::new("FVPJIAOYEDRZXWGCTKUQSBNMHL", &'C');

    let mut ds_configs = DoubleScramblerConfigurations::new(
        vec![i_rotor, ii_rotor, iii_rotor, iv_rotor, v_rotor],
        vec![ukw_a, ukw_b, ukw_c],
    );
    let ds_configs2 =
        DoubleScramblerConfigurations::new(vec![i_rotor, ii_rotor, iii_rotor], vec![ukw_b]);

    let ma = MessageAnalysis::new(
        vec![
            Loop(vec![1, 9]),
            Loop(vec![6, 14, 19]),
            Loop(vec![1, 19, 18, 20, 17]),
        ],
        ds_configs2,
    );
    let sols = ma.search_sols(0);
    println!("{:?}", sols.len());

    for s in sols {
        println!("{}", s);
    }

    let next = ds_configs.next().unwrap();
    println!("{:?}", next);
}
